app.service('SessionService', function ($window) {

    var userRole = "";

    this.createSession = function (headers) {

        if (headers !== null) {
            $window.localStorage.setItem("session", angular.toJson(headers));
        }
    };

    this.destroySession = function () {
        $window.localStorage.removeItem("session");
        $window.localStorage.removeItem("userRole");
    };

    this.isUserAuthenticated = function () {
        return $window.localStorage.getItem("session") !== null;
    };

    this.getAuthorizationHeaders = function () {
        return angular.fromJson($window.localStorage.getItem("session"));
    };

    this.setUserRole = function (role) {
        this.userRole = role;
        $window.localStorage.setItem("userRole", role);
    };

    this.isAdmin = function () {
        this.userRole = $window.localStorage.getItem("userRole");

        return !!(this.userRole != null && this.userRole === "ADMIN");
    };
});

app.factory('authenticationFactory', function (SessionService) {
    return {
        request: function (config) {
            if (SessionService.isUserAuthenticated()) {
                config.headers.authorization = SessionService.getAuthorizationHeaders().authorization;
            }

            return config;
        }
    };
});