(function () {
    "use strict";

    app.controller('userController', userController);

    userController.$inject = ['$scope', '$mdDialog', 'SessionService', '$rootScope', 'UserService', '$location'];

    function userController($scope, $mdDialog, SessionService, $rootScope, UserService, $location) {

        UserService.getUserData()
            .then(function (result) {
                $scope.user = result.data;
            });

        $scope.deleteAccountDialog = function () {
            $mdDialog.show({
                controller: 'userController',
                templateUrl: 'dashboard/modal/account-removal.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                scope: $scope,
                preserveScope: true
            });
        };

        $scope.deleteAccount = function () {
            var password = $scope.account.password;

            UserService.deleteAccount(password)
                .then(function () {
                    $rootScope.authenticated = false;
                    $location.path("/");
                    SessionService.destroySession();
                    $mdDialog.hide();
                }, function () {
                    $scope.removalError = true;
                });
        };

        $scope.editUserDialog = function () {
            $mdDialog.show({
                controller: 'userController',
                templateUrl: 'dashboard/modal/edit-user.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                scope: $scope,
                preserveScope: true
            });
        };

        $scope.editUser = function () {
            UserService.editUser($scope.user)
                .then(function (result) {
                    $scope.user = result.data;
                });

            $mdDialog.hide();
        };

        $scope.changePasswordDialog = function () {
            $mdDialog.show({
                controller: 'userController',
                templateUrl: 'dashboard/modal/change-password.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true
            });
        };

        $scope.confirmPasswordChange = function () {
            UserService.changePassword($scope.password, $scope.newPassword, $scope.repeatedPassword)
                .then(function () {
                    $rootScope.authenticated = false;
                    $location.path("/");
                    SessionService.destroySession();
                }, function () {
                    $scope.changeError = true;
                });

            $mdDialog.hide();
        };
    };
})();