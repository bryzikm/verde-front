(function() {
   "use strict";

    app.service('ProductService', ProductService);

    ProductService.$inject = ['$http', 'AppService'];

    function ProductService ($http, AppService) {
        var service = this;

        service.getAllProducts = getAllProducts;
        service.getAprioriProducts = getAprioriProducts;
        service.getOrderedProducts = getOrderedProducts;
        service.getDeliveryTypes = getDeliveryTypes;
        service.addProductToOrder = addProductToOrder;
        service.confirmOrder = confirmOrder;
        service.removeProduct = removeProduct;
        service.getUserOrders = getUserOrders;
        service.editProductQuantity = editProductQuantity;

        function getAllProducts () {
            return $http.get(AppService.getServerAddress() + '/product/all');
        }

        function getAprioriProducts () {
            return $http.get(AppService.getServerAddress() + '/apriori/products');
        }

        function getOrderedProducts () {
            return $http.get(AppService.getServerAddress() + '/productorder/ordered');
        }

        function getDeliveryTypes () {
            return $http.get(AppService.getServerAddress() + '/delivery/all');
        }

        function addProductToOrder (quantity, product) {
            return $http.post(AppService.getServerAddress() + '/productorder/add/' + quantity, product);
        }

        function confirmOrder (deliveryType) {
            return $http.get(AppService.getServerAddress() + '/order/confirm/' + deliveryType);
        }

        function removeProduct(productId) {
            return $http.delete(AppService.getServerAddress() + '/productorder/delete/' + productId);
        }

        function getUserOrders() {
            return $http.get(AppService.getServerAddress() + '/order/all');
        }

        function editProductQuantity(quantity, productId) {
            return $http.get(AppService.getServerAddress() + '/productorder/edit/' + quantity + '+' + productId);
        }

        return service;
    }
})();