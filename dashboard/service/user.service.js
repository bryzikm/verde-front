(function () {
    "use strict";

    app.service('UserService', UserService);

    UserService.$inject = ['$http', 'AppService'];

    function UserService($http, AppService) {
        var service = this;

        service.getUserData = getUserData;
        service.deleteAccount = deleteAccount;
        service.editUser = editUser;
        service.changePassword = changePassword;

        function getUserData() {
            return $http.get(AppService.getServerAddress() + '/user/logged');
        }

        function deleteAccount(password) {
            return $http.delete(AppService.getServerAddress() + '/user/delete/' + password);
        }

        function editUser(user) {
            return $http.post(AppService.getServerAddress() + '/user/edit', user);
        }

        function changePassword(password, newPassword, repeatedPassword) {
            return $http.get(AppService.getServerAddress() + '/user/editpassword/' + password + '+' + newPassword + '+' + repeatedPassword)
        }

        return service;
    }
})();