(function () {
    "use strict";

    app.controller('productCtrl', ProductCtrl);

    ProductCtrl.$inject = ['$scope', '$mdDialog', 'ProductService', 'AdminService'];

    function ProductCtrl($scope, $mdDialog, ProductService, AdminService) {
        ProductService.getAllProducts()
            .then(function (result) {
                $scope.products = result.data;
            });

        ProductService.getAprioriProducts()
            .then(function (result) {
                $scope.recommendedProducts = result.data;
            });

        ProductService.getOrderedProducts()
            .then(function (result) {
                $scope.orderedProducts = result.data;
                $scope.emptyCard = false;
            }, function () {
                $scope.emptyCard = true;
            });

        ProductService.getDeliveryTypes()
            .then(function (result) {
                $scope.deliveryTypes = result.data;
            });

        ProductService.getUserOrders()
            .then(function (result) {
                $scope.orders = result.data;
            }, function () {
                $scope.emptyOrders = true;
            });

        $scope.orderValue = function () {
            var sum = 0.0;

            if ($scope.orderedProducts && !$scope.emptyCard) {
                for (var i = 0; i < ($scope.orderedProducts).length; i++) {
                    sum += parseFloat($scope.orderedProducts[i].value);
                }
            }

            return Math.round(sum * 100) / 100;
        };

        $scope.addProductToOrder = function (product) {
            $scope.openOrderModal(product);
        };

        $scope.editQuantityModal = function (mappedProduct) {
            $scope.editQuantityProduct = mappedProduct;

            $mdDialog.show({
                controller: 'productCtrl',
                templateUrl: 'dashboard/modal/quantity-edit.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                scope: $scope,
                preserveScope: true
            });
        };

        $scope.editQuantity = function () {
            ProductService.editProductQuantity($scope.edittedQuantity.quantity, $scope.editQuantityProduct.product.id)
                .then(function (result) {
                    $scope.orderedProducts = result.data;
                });

            $mdDialog.hide();
        };

        $scope.removeProduct = function (mappedProduct) {
            ProductService.removeProduct(mappedProduct.product.id)
                .then(function (result) {
                    $scope.orderedProducts = result.data;

                    if ($scope.orderedProducts && ($scope.orderedProducts).length == 0) {
                        $scope.emptyCard = true;
                    }
                });
        };

        $scope.openErrorModal = function () {
            $mdDialog.show({
                controller: function ($scope, $mdDialog) {
                    $scope.ok = function () {
                        $mdDialog.hide();
                    };
                },
                templateUrl: 'dashboard/modal/order-confirmation.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true
            });
        };

        $scope.openOrderModal = function (product) {
            $scope.dialogProduct = product;

            $mdDialog.show({
                controller: 'productCtrl',
                templateUrl: 'dashboard/modal/quantity-choice.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                scope: $scope,
                preserveScope: true,
                locals: {dialogProduct: product},
                bindToController: true
            });
        };

        $scope.addProduct = function () {
            var quantity = $scope.productQuantity.quantity;
            var product = $scope.dialogProduct;

            ProductService.addProductToOrder(quantity, product)
                .then(function (result) {
                    $scope.orderedProducts = result.data;
                    $scope.emptyCard = false;
                    $mdDialog.hide();
                }, function () {
                    $scope.addingErrorShow = true;
                });
        };

        $scope.deliveryTypeModal = function () {
            $mdDialog.show({
                controller: 'productCtrl',
                templateUrl: 'dashboard/modal/delivery-type.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                scope: $scope,
                preserveScope: true
            });
        };


        $scope.confirmOrder = function () {
            var delivery = $scope.order.orderType;

            ProductService.confirmOrder(delivery)
                .then(function () {
                    $scope.orderedProducts = [];
                    $scope.emptyCard = true;
                });

            $mdDialog.hide();
        };

        $scope.listOrderProducts = function (order) {
            $mdDialog.show({
                templateUrl: 'dashboard/modal/client-product-list.html',
                clickOutsideToClose: true,
                controller: function ($scope) {
                    AdminService.getOrderProducts(order.id)
                        .then(function (result) {
                            $scope.products = result.data;
                            console.log(result.data)
                        });
                }
            })
        };
    }
})();