$(document).ready(function () {

    $(document).on('click', '#button--mobile', function () {
       if($('#header .button__bar').is(':hidden')) {
           $('#header .button__bar').addClass('show');
       } else {
           $('#header .button__bar').removeClass('show');
       }
    });

    $(window).on('resize', function () {
        $('#header .button__bar').removeClass('show');
    });

    $(document).on('click', '#header .button__bar.show .button', function () {
        $('#header .button__bar').removeClass('show');
    });
});