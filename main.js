var app = angular.module('main', ['ngMaterial', 'ngRoute', 'ngMessages']);

app.config(function ($routeProvider, $httpProvider) {

    $routeProvider.when('/', {
        templateUrl: 'home/home.html',
        loginRequire: false
    }).when('/dashboard', {
        templateUrl: 'dashboard/dashboard.html',
        controller: 'productCtrl',
        loginRequire: true
    }).when('/admin', {
        templateUrl: 'admin/admin.html',
        controller: 'adminCtrl',
        loginRequire: true,
        adminRequire: true
    }).otherwise('/');

    $httpProvider.interceptors.push('authenticationFactory');
});

app.run(function ($rootScope, $location, SessionService) {

    $rootScope.$on("$routeChangeStart", function (event, newUrl) {

        if (newUrl.loginRequire && !SessionService.isUserAuthenticated()) {
            alert("Musisz być zalogowany!");
            $location.path("/");
        }

        if (newUrl.adminRequire && !SessionService.isAdmin()) {
            alert("Tylko dla admina!!!");
            $location.path("/");
        }
    });
});

app.controller('navigation', function ($rootScope, $scope, $mdDialog, $location, $http, $window, SessionService) {

    $window.onscroll = function () {
        var topScrolling = document.body.scrollTop || document.documentElement.scrollTop;
        var header = angular.element(document.querySelector('#header'));

        if(topScrolling > 50) {
            header.addClass('fixed');
        } else {
            header.removeClass('fixed');
        }
    };

    $scope.openLoginModal = function () {
        $mdDialog.show({
            controller: function ($scope, $mdDialog) {

            },
            templateUrl: 'main/modal/login.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true
        });
    };

    $scope.openPasswordResetModal = function () {
        $mdDialog.show({
            controller: function ($scope, $mdDialog) {
                $scope.resetPassword = function () {
                    var username = $scope.user.username;

                    $http.get('http://localhost:8082/user/reset/' + username)
                        .then(function () {
                            $mdDialog.hide();
                        }, function () {
                            $scope.resetError = true;
                        });
                }
            },
            templateUrl: 'main/modal/password-reset.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true
        });
    };

    $scope.openRegisterModal = function (event) {
        $mdDialog.show({
            controller: function ($scope, $mdDialog) {
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.answer = function (answer) {
                    $mdDialog.hide(answer);
                };
            },
            templateUrl: 'main/modal/register.html',
            parent: angular.element(document.body),
            targetEvent: event,
            clickOutsideToClose: true
        });
    };

    var authenticate = function (credentials, callback) {
        $rootScope.headers = credentials ? {authorization: "Basic " + btoa(credentials.username + ":" + credentials.password)} : {};

        $http.get('http://localhost:8082/user', {headers: $rootScope.headers})
            .success(function (data) {
                if (data.name) {
                    $rootScope.authenticated = true;
                    SessionService.createSession($rootScope.headers);
                    SessionService.setUserRole(data.authorities['0'].authority);
                    $rootScope.admin = SessionService.isAdmin();
                } else {
                    $rootScope.authenticated = false;
                }
                callback && callback();
            }).error(function () {
                $rootScope.authenticated = false;
                $rootScope.admin = false;
                callback && callback();
        });
    };

    $rootScope.authenticated = SessionService.isUserAuthenticated();
    $rootScope.admin = SessionService.isAdmin();

    if (!$scope.credentials) {
        $scope.credentials = {};
    }

    $scope.loginUser = function () {
        authenticate($scope.credentials, function () {
            if ($rootScope.authenticated) {
                $location.path("/dashboard");
                $mdDialog.hide();
            } else {
                $location.path("/");
                $scope.loginError = true;
            }
        });
    };

    $scope.logout = function () {
        $rootScope.authenticated = false;
        $rootScope.admin = false;
        $location.path("/");
        SessionService.destroySession();
    }
});

app.controller('register', function ($scope, $http) {

    $scope.registerUser = function () {

        $http({
            method: 'POST',
            url: 'http://localhost:8082/user/register',
            data: $scope.user
        }).success(function () {
            $scope.registerMessage = "Użytkownik został zarejestrowany";
            $scope.registerMessageShow = true;
        }).error(function () {
            $scope.registerMessage = "Podany adres e-mail jest już zajety!";
            $scope.registerMessageShow = true;
        })
    }
});