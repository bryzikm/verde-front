app.service('AdminService', function ($http, AppService) {

    this.getSolicitedOrders = function () {
        return $http.get(AppService.getServerAddress() + '/order/solicited');
    };

    this.getOrderProducts = function (orderId) {
        return $http.get(AppService.getServerAddress() + '/productorder/order/' + orderId);
    };

    this.markOrderAsCompleted = function (order) {
        return $http.post(AppService.getServerAddress() + '/order/complete', order);
    };

    this.getArchivedOrders = function () {
        return $http.get(AppService.getServerAddress() + '/order/archived');
    };

    this.deleteArchivedOrder = function (order) {
        return $http.delete(AppService.getServerAddress() + '/order/delete/' + order.id);
    };

    this.getAllProducts = function () {
        return $http.get(AppService.getServerAddress() + '/product/all');
    };

    this.addProduct = function (product) {
        return $http.post(AppService.getServerAddress() + '/product/add', product);
    };

    this.editProduct = function (product) {
        return $http.post(AppService.getServerAddress() + '/product/edit', product);
    };

    this.deleteProduct = function (product) {
        return $http.delete(AppService.getServerAddress() + '/product/delete/' + product.id);
    };

    this.getCategories = function () {
        return $http.get(AppService.getServerAddress() + '/category/all');
    };

    this.getUnits = function () {
        return $http.get(AppService.getServerAddress() + '/unit/all');
    };

    this.checkIfListIsEmpty = function(length) {
        return length == 0;
    }
});