app.controller('adminCtrl', function ($scope, $location, $mdDialog, AdminService) {

    AdminService.getSolicitedOrders()
        .then(function (result) {
            $scope.orders = result.data;
            $scope.emptyOrders = false;
        }, function () {
            $scope.emptyOrders = true;
        });

    AdminService.getArchivedOrders()
        .then(function (result) {
            $scope.archivedOrders = result.data;
            $scope.emptyArchivedOrders = false;
        }, function () {
            $scope.emptyArchivedOrders = true;
        });

    AdminService.getAllProducts()
        .then(function (result) {
            $scope.products = result.data;
            $scope.emptyProducts = false;
        }, function () {
            $scope.emptyProducts = true;
        });

    AdminService.getCategories()
        .then(function (result) {
            $scope.categories = result.data;
        });

    AdminService.getUnits()
        .then(function (result) {
            $scope.units = result.data;
        });

    $scope.listOrderProducts = function (order) {
        $mdDialog.show({
            templateUrl: 'admin/modal/order-sheet.html',
            clickOutsideToClose: true,
            controller: function ($scope) {
                AdminService.getOrderProducts(order.id)
                    .then(function (result) {
                        $scope.products = result.data;
                    });
            }
        })
    };

    $scope.markAsCompleted = function (order) {
        AdminService.markOrderAsCompleted(order)
            .then(function (result) {
                $scope.orders = result.data.orders;
                $scope.archivedOrders = result.data.archivedOrders;
            });
    };

    $scope.deleteArchivedOrder = function (order) {
        AdminService.deleteArchivedOrder(order)
            .then(function (result) {
                $scope.archivedOrders = result.data;

                if(result.data.length === 0) {
                    $scope.emptyArchivedOrders = true;
                }
            });
    };

    $scope.deleteProduct = function (product) {
        AdminService.deleteProduct(product)
            .then(function (result) {
                $scope.products = result.data;
                $scope.emptyProducts = AdminService.checkIfListIsEmpty(result.data.length);
            });
    };

    $scope.addProductModal = function () {
        $mdDialog.show({
            controller: 'adminCtrl',
            templateUrl: 'admin/modal/add-product.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            scope: $scope,
            preserveScope: true
        });
    };

    $scope.addProduct = function () {
        AdminService.addProduct($scope.newProduct)
            .then(function (result) {
                $scope.products = result.data;
            });

        $mdDialog.hide();
    };

    $scope.editProductModal = function (product) {
        $scope.product = product;

        $mdDialog.show({
            controller: 'adminCtrl',
            templateUrl: 'admin/modal/edit-product.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            scope: $scope,
            preserveScope: true
        });
    };

    $scope.editProduct = function () {
        AdminService.editProduct($scope.product)
            .then(function (result) {
                $scope.products = result.data;
            });

        $mdDialog.hide();
    };
});